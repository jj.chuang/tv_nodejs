'use strict';
let fs = require('fs');
var express = require('express');
var app = express();
var port = process.env.PORT || 80;
var path = require('path');

let ccxt = require('ccxt');
console.log(ccxt.exchanges); // print all available exchanges

let delay = 2000; // milliseconds = seconds * 1000
// let delay = 20000;
(async () => {
    let kraken = new ccxt.kraken({ verbose: false }); // log HTTP requests

    // await kraken.load_markets() // request markets
    // console.log(kraken.id, kraken.markets)    // output a full list of all loaded markets
    // console.log(Object.keys(kraken.markets)) // output a short list of market symbols
    // console.log(kraken.markets['BTC/USD'])    // output single market details

    
    while (true) {
        // console.log(kraken.id, await kraken.fetchOrderBook(kraken.symbols[7]), {
        /*
        let orderBook = await kraken.fetchOrderBook('BTC/USD')
        console.log(orderBook, {
            'limit_bids': 5, // max = 50
            'limit_asks': 5, // may be 0 in which case the array is empty
            'group': 1, // 1 = orders are grouped by price, 0 = orders are separate
        });
        */
        var ticker = await kraken.fetchTicker('BTC/USD');
        console.log(kraken.id, ticker);
        // fs.writeFile('kraken_ticker.txt', String(ticker), function (err) {
        /*
        fs.appendFile('kraken_ticker.txt', JSON.stringify(ticker), function (err) {
            if (err) throw err;
            console.log('Updated!');
        });
        */
        await new Promise(resolve => setTimeout(resolve, delay)); // rate limit
    }
    
    // console.log(await kraken.fetchTrades('BTC/USD'));
    // console.log(await kraken.fetchOHLCV('BTC/USD', '1d'));

    /*
    console.log("RateLimit: " + kraken.rateLimit);
    let sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
    if (kraken.hasFetchOHLCV) {
        (async () => {
            for (symbol in kraken.markets) {
                await sleep(kraken.rateLimit) // milliseconds
                console.log(await kraken.fetchOHLCV(symbol, '1m')) // one minute
            }
        })()
    }
    */
})()



app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(port, '127.0.0.1', function () {
    console.log('HTTP server running on http://127.0.0.1/');
});

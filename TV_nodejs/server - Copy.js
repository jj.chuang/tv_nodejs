'use strict';
var http = require('http');
var port = process.env.PORT || 1337;
/*
// var jquery = require('//code.jquery.com/jquery-1.11.2.min.js');
var script = document.createElement('script');
script.src = 'http://code.jquery.com/jquery-1.11.2.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);
var TradingView = require('./charting_library/charting_library.min');
// var TradingView = require('./charting_library/tv');
var Datafeeds = require('./charting_library/datafeed/udf/datafeed');
*/

/*
function onready() {
    var widget = global.tvWidget = new TradingView.widget({
    fullscreen: true,
    autosize: true,
    symbol: 'AAPL',
    interval: 'D',
    container_id: "tv_chart_container",
    //	BEWARE: no trailing slash is expected in feed URL
    datafeed: new Datafeeds.UDFCompatibleDatafeed("https://demo_feed.tradingview.com"),
    library_path: "charting_library/",
			    // locale: getParameterByName('lang') || "en",
    locale: "en",
    //	Regression Trend-related functionality is not implemented yet, so it's hidden for a while
    drawings_access: { type: 'black', tools: [{ name: "Regression Trend" }] },
    disabled_features: ["use_localstorage_for_settings"],
    enabled_features: ["study_templates"],
    charts_storage_url: 'http://saveload.tradingview.com',
    charts_storage_api_version: "1.1",
    client_id: 'tradingview.com',
    user_id: 'public_user_id'
	});
};

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello World\n');

    onready();
}).listen(port);
*/

var fs = require('fs');

// fs.readFileSync('./index.html', 'utf-8', function (err, html) {
fs.readFile('./index - Copy.html', function (err, html) {

    if (err) throw err;

    http.createServer(function (request, response) {
        response.writeHeader(200, { "Content-Type": "text/html" });
        response.write(html);
        response.end();
        // response.end('Hello World\n');
        console.log('This example is different!');
    }).listen(port);
});
